﻿using System.Collections.Generic;

namespace MathExpressionCalculator.Interfaces
{
    public interface IInputOutput
    {
        public List<string> GetInput();
        public void PrintOutput(List<string> output);
    }
}
