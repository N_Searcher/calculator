﻿using System;
using System.Collections.Generic;

namespace MathExpressionCalculator
{
    public class Calculator
    {
        private const string _openningBraceSign = "(";
        private const string _closingBraceSign = ")";
        private delegate double _mathAction(double a, double b);
        private readonly Dictionary<string, _mathAction> _mathActions = new Dictionary<string, _mathAction>() {
            { "+", (a, b) => a += b },
            { "-", (a, b)=> a -= b },
            { "/", (a, b)=> a /= b },
            { "*", (a, b)=> a *= b }
        };
        private readonly Dictionary<string, int> _operations = new Dictionary<string, int>() {
            { "+", 1 },
            { "-", 1 },
            { "/", 2 },
            { "*", 2 },
        };

        public string CalculateExpression(string expression, bool shouldBeRounded = true, int signQuantity = 2)
        {
            var mathExpression = new MathExpression(expression);

            if (!mathExpression.IsExpressionValid)
                return "invalid expression";
            else
            {
                try
                {
                    double result = shouldBeRounded ? Math.Round(MakeMathOperations(mathExpression), signQuantity) : MakeMathOperations(mathExpression);

                    return result.ToString();
                }
                catch (DivideByZeroException)
                {
                    return "Zero division error";
                }
                catch (Exception error)
                {
                    return error.Message;
                }
            }
        }

        public List<string> CalculateExpressions(List<string> expressions, bool shouldBeRounded = false, int signQuantity = 2)
        {
            var expressionResults = new List<string>();

            foreach (var item in expressions)
            {
                expressionResults.Add(CalculateExpression(item, shouldBeRounded, signQuantity));
            }

            return expressionResults;
        }

        private double MakeMathOperations(MathExpression mathExpr)
        {
            var numbers = new Stack<double>();
            var expressionOperation = new Stack<string>();

            foreach (var item in mathExpr.SplittedExpression)
            {
                if (double.TryParse(item, out double number))
                    numbers.Push(number);
                else if (ExpressionOperationAddition(expressionOperation, item))
                    expressionOperation.Push(item);
                else
                    ExecuteExpression(numbers, expressionOperation, item);
            }

            ExecuteExpression(numbers, expressionOperation);

            return numbers.Pop();
        }

        private void ExecuteExpression(Stack<double> numbers, Stack<string> expressionOperation, string operand = "")
        {
            double leftOperand, rightOperand;

            //Make some math till true
            while (ExpressionExecutionNecessity(expressionOperation, operand))
            {
                rightOperand = numbers.Pop();
                leftOperand = numbers.Pop();

                if(double.IsInfinity(_mathActions[expressionOperation.Peek()](leftOperand, rightOperand)))
                    throw new DivideByZeroException();
                
                numbers.Push(_mathActions[expressionOperation.Pop()](leftOperand, rightOperand));
            }

            if (!string.IsNullOrEmpty(operand))
            {
                //Remove last element from Stack if it's openning brace
                if (expressionOperation.Count != 0 && expressionOperation.Peek() == _openningBraceSign && operand == _closingBraceSign)
                    expressionOperation.Pop();
                //Add operand to Stack if it's not closing brace
                if (operand != _closingBraceSign)
                    expressionOperation.Push(operand);
            }
        }

        private bool ExpressionOperationAddition(Stack<string> exprOperations, string operation)
        {
            return operation == _openningBraceSign && exprOperations.Count == 0 && PriorityChecking(exprOperations, operation) && exprOperations.Peek() == _openningBraceSign;
        }

        private bool PriorityChecking(Stack<string> exprOperations, string operation)
        {
            return _operations.ContainsKey(operation) && _operations.ContainsKey(exprOperations.Peek()) && _operations[operation] > _operations[exprOperations.Peek()];
        }

        private bool ExpressionExecutionNecessity(Stack<string> exprOperations, string operand)
        {
            if (exprOperations.Count > 0)
            {
                if (_operations.ContainsKey(operand) && _operations.ContainsKey(exprOperations.Peek()))
                    if (_operations[operand] <= _operations[exprOperations.Peek()])
                        return true;
                if (operand == _closingBraceSign && exprOperations.Peek() != _openningBraceSign)
                    return true;
                if (operand == "")
                    return true;
            }

            return false;
        }
    }
}
