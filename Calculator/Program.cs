﻿using System;
using System.Collections.Generic;
using MenuLib;
using MathExpressionCalculator.Interfaces;

namespace MathExpressionCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            var calculator = new Calculator();
            IInputOutput inputOutput;
            List<string> mathData;

            if (ShowMenu() == DataSource.Console)
            {
                inputOutput = new ConsoleIO();
                mathData = GetUsersInfo("Print math expression: ");
            }
            else
            {
                inputOutput = new FileIO(GetUsersInfo("Print input file path: ")[0], GetUsersInfo("Print output file path: ")[0]);
                mathData = inputOutput.GetInput();
            }

            inputOutput.PrintOutput(MakeCalculations(inputOutput, calculator, mathData));

            Console.ReadKey();
        }

        private static List<string> MakeCalculations(IInputOutput inputOutput, Calculator calculator, List<string> input)
        {
            var results = calculator.CalculateExpressions(input);

            for (var i = 0; i < input.Count; i++)
            {
                input[i] += $" = {results[i]}";
            }

            return input;
        }

        private static List<string> GetUsersInfo(string message)
        {
            var consoleIO = new ConsoleIO();
            consoleIO.PrintOutput(new List<string>() { message });

            return consoleIO.GetInput();
        }

        private static DataSource ShowMenu()
        {
            var menu = new ConsoleMenu(new string[] { " Get data from file.", " Get data from console." });

            return menu.ShowStartMenu() == 0 ? DataSource.File : DataSource.Console;
        }
    }
}
