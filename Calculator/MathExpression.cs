﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;

namespace MathExpressionCalculator
{
    public class MathExpression
    {
        private const string _minusSign = "-";
        private const string _openningBraceSign = "(";
        private const string _closingBraceSign = ")";
        private string _expression;

        public List<string> SplittedExpression { get; private set; }
        public bool IsExpressionValid { get; private set; }

        public string Expression
        {
            get
            {
                return _expression;
            }
            set
            {
                _expression = value;
            }
        }

        public MathExpression(string fullExpression)
        {
            Expression = fullExpression;

            IsExpressionValid = ValidateExpression();

            if (!IsExpressionValid)
                return;

            SplittedExpression = SplitExpression();
        }

        private List<string> SplitExpression()
        {
            var splittedExpression = new List<string>(Regex.Split(Expression, @"^(-\d)|([/*+()])|(-)").Where(item => !string.IsNullOrEmpty(item)).ToList());

            for (int i = 1; i < splittedExpression.Count; i++)
            {
                if (splittedExpression[i] == _minusSign && Regex.IsMatch(splittedExpression[i - 1], @"[\-+/*(]"))
                {
                    splittedExpression[i + 1] = _minusSign + splittedExpression[i + 1];
                    splittedExpression.RemoveAt(i);
                }
            }

            return splittedExpression;
        }

        private bool ValidateExpression()
        {
            var expressionTemplate = Regex.IsMatch(Expression, @"^[+/*]|[^+-\/*\d()]|\d\(|\)\d|\(\d*\)|\)\d*\(|\((\+|\-|\/|\*)\)|\*{2,}|\/{2,}|\+{2,}|[\-+/*][+/*]|-{2,}[^\d]");
            var scopeCount = Regex.Matches(Expression, @"\(").Count == Regex.Matches(Expression, @"\)").Count;
            var scopeSequence = Expression.IndexOf(_closingBraceSign) < Expression.IndexOf(_openningBraceSign);

            return !expressionTemplate & scopeCount & !scopeSequence;
        }
    }
}
