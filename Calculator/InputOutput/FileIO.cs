﻿using System;
using System.Collections.Generic;
using MathExpressionCalculator.Interfaces;
using System.IO;

namespace MathExpressionCalculator
{
    public class FileIO : IInputOutput
    {
        private string _inputFilePath;
        private string _outputFilePath;

        public string InputFilePath
        {
            get
            {
                return _inputFilePath;
            }
            set
            {
                _inputFilePath = File.Exists(value) ? value : throw new Exception("Invalid input file path.");
            }
        }

        public string OutputFilePath 
        {
            get
            {
                return _outputFilePath;
            }
            set
            {
                _outputFilePath = File.Exists(value) ? value : throw new Exception("Invalid output file path.");
            }
        }

        public FileIO(string inPath, string outPath)
        {
            InputFilePath = inPath;
            OutputFilePath = outPath;
        }

        public List<string> GetInput()
        {
            return new List<string>(File.ReadAllLines(InputFilePath));
        }

        public void PrintOutput(List<string> output)
        {
            File.WriteAllLines(OutputFilePath, output);
        }
    }
}
