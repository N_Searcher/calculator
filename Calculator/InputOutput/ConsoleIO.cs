﻿using System;
using System.Collections.Generic;
using MathExpressionCalculator.Interfaces;

namespace MathExpressionCalculator
{
    public class ConsoleIO : IInputOutput
    {
        public List<string> GetInput()
        {
            return new List<string>(new string[] { Console.ReadLine() });
        }

        public void PrintOutput(List<string> output)
        {
            foreach (var item in output)
            {
                Console.WriteLine(item);
            }
        }
    }
}
