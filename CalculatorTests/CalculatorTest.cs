﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MathExpressionCalculator;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;

namespace CalculatorTests
{
    [TestClass]
    public class CalculatorTest
    {
        [TestMethod]
        public void CalculateExpressionTest()
        {
            var calculator = new Calculator();
            double expected = double.Parse("-221.12", new CultureInfo("en-US"));
            double actual = double.Parse(calculator.CalculateExpression("51-(622*7/16)"));
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void CalculateMultipleExpressionsTest()
        {
            var calculator = new Calculator();
            var results = calculator.CalculateExpressions(new List<string>() { "(92-(12*5+2))/2*10", "(45--5)/2" });

            Assert.IsTrue(results.SequenceEqual(new List<string>() { "150", "25" }));
        }
    }
}
