using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using MathExpressionCalculator;

namespace CalculatorTests
{
    [TestClass]
    public class MathExpressionTests
    {
        [DataTestMethod]
        [DataRow("(1-2-(25-40)")]
        [DataRow("1-2-(25-+40)")]
        [DataRow(")1-2-(25-40)(")]
        [DataRow("(1-*e*2-(25-40)")]
        [DataRow("200/()")]
        public void ExpressionInvalidityTest(string mathExprn)
        {
            var expression = new MathExpression(mathExprn);
            Assert.IsTrue(!expression.IsExpressionValid);
        }

        [TestMethod]
        public void SplitExpressionTest()
        {
            var expression = new MathExpression("11+(((4045-45)/2)-100)/4");
            var splittedStr = new List<string>(new string[] { "11","+","(", "(", "(", "4045", "-", "45", ")", "/", "2", ")", "-", "100", ")", "/", "4", });

            Assert.IsTrue(expression.SplittedExpression.SequenceEqual(splittedStr));
        }
    }
}
